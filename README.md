# rfc

A utility to search, view and download RFCs.

```
Usage:

    rfc [--generate] [--search term] [--view number] [--fetch number]
        --generate - Generate a local database file.
        --search   - Search the local or remote database for a keyword.
        --view     - Fetch and view an RFC specified, by number.
        --fetch    - Fetch an RFC, specified by number.
```


